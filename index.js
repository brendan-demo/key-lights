const axios = require('axios');
const argv = require('minimist')(process.argv.slice(2));

//console.log(argv)

const lightIPs = [
    '192.168.2.113',
    '192.168.2.242',
    '192.168.2.243'
]
const lightPort = 9123


async function getInfo() {
    const one = await axios.get(`http://${lightIPs[0]}:${lightPort}/elgato/accessory-info`)
    console.log(one)
}

async function change(lightIP, on = true) {
    const val = on ? 1 : 0;
    const res = await axios.put(
        `http://${lightIP}:${lightPort}/elgato/lights`,
        {
            "numberofLights": 1,
            lights: [
                { "on": val }
            ]
        }
    )
}

if (argv.alloff) {
    lightIPs.forEach(v => change(v, false));
}

if (argv.allon) {
    lightIPs.forEach(v => change(v, true));
}